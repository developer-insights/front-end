const assert = require('assert');
const frontEnd = require('../index.js');

describe('frontEnd', () => {
  it('has a test', () => {
    assert.equal(frontEnd.sayHello(), 'Hello');
  });
});
